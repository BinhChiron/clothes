<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<html>
<head>

    <meta charset="utf-8">
    <title>Giới thiệu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta content="Themesbrand" name="author">
    <jsp:include page="/WEB-INF/views/front-end/common/css.jsp"></jsp:include>
</head>

<body>

<jsp:include page="/WEB-INF/views/front-end/common/header.jsp"></jsp:include>
<div class="bg-black-menu">
    <div class="container">
        <div class="menu">
				<span id="icon-close" class="d-md-none"><i
                        class="fas fa-times"></i></span>
            <ul>
                <li>
                    <div class="search d-flex d-md-none d-block">
                        <div>
                            <input type="text" placeholder="Tìm kiếm...">
                        </div>
                        <div>
                            <button>
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </li>
                <li><a href="${pageContext.request.contextPath}/">Trang
                    Chủ</a></li>
                <li class="active"><a
                        href="${pageContext.request.contextPath}/gioithieu">Giới Thiệu</a></li>
                <li><a href="${pageContext.request.contextPath}/quanAo">Sản
                    phẩm</a></li>
                <li><a href="${pageContext.request.contextPath}/blog">Blogs</a></li>
                <li><a href="${pageContext.request.contextPath}/contact">Liên
                    Hệ</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
<div class="container">
    <div class="row mt-5">
        <div class="col-12 col-md-6" data-aos="fade-right">
            <img src="../image/shop-quan-ao.jpg" width="100%" height="450px">
        </div>
        <div class="col-12 col-md-6 pt-5 content-about" data-aos="fade-left">
            <h2>Giới thiệu về Shop Clothes</h2>
            <p>Ở <strong>Clothes</strong>, mọi chiến lược đều bắt đầu và tập trung cho sản phẩm. Các sản phẩm của
                Coolmate từ khâu dệt vải, nhuộm vải, cắt may, hoàn thiện đều được thực hiện trong những nhà máy sản xuất
                địa phương đạt tiêu chuẩn xuất khẩu và bởi những bàn tay khéo léo với cái tâm mang đến những sản phẩm
                được gắn nhãn <span class="text--italic text--bold">“Tự hào sản xuất tại Việt Nam”</span>. Clothes luôn
                đi kèm triết lý <span
                        class="text--italic text--bold">“Các đối tác sản xuất là một phần của Clothes”</span> và chúng
                tôi tập trung xây dựng mối quan hệ để cùng nhau nâng cao chất lượng sản phẩm, có nhiều đơn hàng và tạo
                nhiều việc làm hơn. Các sản phẩm của Coolmate đã được tạo ra như thế nào, cùng Clothes tìm hiểu <a
                        href="${pageContext.request.contextPath}/"
                        class="text--primary">tại đây</a> nhé!</p>
        </div>
    </div>
</div>
<div class="container">
    <div class="row about-service">
        <div class="col-12 col-sm-6 col-md-4 d-flex" data-aos="fade-left">
            <i class="fas fa-star-and-crescent"></i>
            <div class="title">
                <h3>Hàng chính hãng</h3>
                <p>Clothes luôn
                    đi kèm triết lý <span
                            class="text--italic text--bold">“Các đối tác sản xuất là một phần của Clothes”</span> và
                    chúng
                    tôi tập trung xây dựng mối quan hệ để cùng nhau nâng cao chất lượng sản phẩm, có nhiều đơn hàng và
                    tạo
                    nhiều việc làm hơn.</p>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4 d-flex" data-aos="fade-left">
            <i class="fas fa-child"></i>
            <div class="title">
                <h3>Sản phẩm mới</h3>
                <p><strong>Coolmate</strong> là sự kết hợp giữa ‘Cool’ và ‘Mate’. Chúng tôi mong muốn đem đến cho khách
                    hàng ưa trải nghiệm thích phiêu lưu của mình một vẻ ngoài “cool” ngầu, mới mẻ, năng động. Hơn cả,
                    chúng tôi muốn được trở thành những người bạn, những người đồng hành “mate” trong suốt những ngày
                    bận rộn, đem lại sự thoải mái và tự tin trong từng bước di chuyển, thấu hiểu và trở thành những trợ
                    thủ đắc lực nhất.</p>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4 d-flex" data-aos="fade-left">
            <i class="fas fa-hand-holding-medical"></i>
            <div class="title">
                <h3>BH 60 ngày</h3>
                <p class="animated fadeInRight"><strong>Clothes</strong> đem lại sự thoải mái nhất trong mua sắm. Bạn
                    có thể tự do xem bất kỳ món hàng nào, theo dõi những món đồ muốn mua, mua hàng trong tích tắc và
                    thậm chí là có thể đổi trả <strong>60 ngày</strong> miễn phí vì bất kỳ lý do gì.</p>
            </div>
        </div>
        <div class="col-12 col-6 col-md-4 d-flex " data-aos="fade-left">
            <i class="fas fa-history"></i>
            <div class="title">
                <h3>Đổi trả trong vòng 7 ngày</h3>
                <p class="animated fadeInRight"><strong>Clothes</strong> đem lại sự thoải mái nhất trong mua sắm. Bạn
                    có thể tự do xem bất kỳ món hàng nào, theo dõi những món đồ muốn mua, mua hàng trong tích tắc và
                    thậm chí là có thể đổi trả <strong>60 ngày</strong> miễn phí vì bất kỳ lý do gì.</p>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4 d-flex " data-aos="fade-left">
            <i class="fas fa-shipping-fast"></i>
            <div class="title">
                <h3>Miễn phí giao hàng</h3>
                <p class="animated fadeInLeft">Quên đi những khoản chi trả kha khá và phải đi nhiều nơi lẻ tẻ để có được
                    những món đồ cơ bản nhất như áo thun, quần short, quần sịp, tất (vớ), chúng tôi mong muốn thay đổi
                    mọi thứ. Chỉ bằng vài cú click chuột và một tủ đồ đầy đủ sẽ đến gõ cửa nhà bạn ngay sau đó.</p>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4 d-flex " data-aos="fade-left">
            <i class="fas fa-hand-holding-usd"></i>
            <div class="title">
                <h3>Giá cả hợp lí</h3>
                <p class="animated fadeInRight">Tất cả những gì bạn thấy trên web là tất cả những gì bạn phải trả, cam
                    kết không có chi phí phát sinh trong quá trình mua và đổi trả hàng.</p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid pt-5 p-0">
    <div class="banner-des d-flex align-items-center">
        <div class="container">
            <div class="row text-center">
                <div class="col-6 col-sm-3">
                    <div class="banner-content">
                        <div>
                            <h2 class="num">12099</h2>
                        </div>
                        <p>Sản phẩm</p>
                    </div>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="banner-content">
                        <div>
                            <h2 class="num">12</h2>
                        </div>
                        <p>Giải thưởng</p>
                    </div>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="banner-content">
                        <div>
                            <h2 class="num">120589</h2>
                        </div>
                        <p>Khách hàng hài lòng</p>
                    </div>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="banner-content">
                        <div>
                            <h2 class="num">2525</h2>
                        </div>
                        <p>Cửa hàng chi nhánh</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row about-service">
        <div class="col-12 col-sm-6 col-md-4 " data-aos="zoom-in-up">
            <div class="image">
                <img src="../image/logo.jpg" width="30%" height="100">
            </div>
            <div class="title2">
                <p>Tận tình chăm sóc khách hàng</p>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4 " data-aos="zoom-in-up">
            <div class="image">
                <img src="../image/logo.jpg" width="30%" height="100">
            </div>
            <div class="title2">
                <p>Mang lại sản phẩm tốt nhất</p>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4" data-aos="zoom-in-up">
            <div class="image">
                <img src="../image/logo.jpg" width="30%" height="100">
            </div>
            <div class="title2">
                <p>Trải nghiệm mua sắm thông minh</p>
            </div>
        </div>
    </div>
</div>

<jsp:include page="/WEB-INF/views/front-end/common/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/front-end/common/js.jsp"></jsp:include>
<script type="text/javascript">
    $(".num").counterUp({
        delay: 10,
        time: 1000
    });
</script>
</body>
</html>