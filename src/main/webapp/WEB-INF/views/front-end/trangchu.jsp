<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>

<!-- JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Store Clothes</title>
    <jsp:include page="/WEB-INF/views/front-end/common/css.jsp"></jsp:include>
    ${message }
    <style>
        .desc-sale {
            position: absolute;
            top: 0;
            right: 3px;
            background: url(../image/icosale.png);
            width: 50px;
            height: 50px;
        }

        .container-items .desc-sale {
            top: 8px;
            right: 8px;
        }

        .desc-sale span {
            transform: rotate(45deg);
            position: absolute;
            top: 5px;
            right: 0px;
            font-weight: bold;
        }

        .cost1 {
            font-weight: bold;
            font-size: 18px;
        }

        .item:hover {
            color: #FF6633;
        }

        .item a:hover {
            text-decoration: none;
        }

        .text-outer a:hover {
            text-decoration: none;
        }

        .text-outer a {
            color: #000000;
        }

        .page-item.first, .page-item.last {
            display: none;
        }

        .pagesss {
            float: right;
        }

        .item {
            margin: 50px 10px 20px 10px;
        }

        .item .item-title {
            padding: 10px;
            min-height: 100px;
            color: black;
        }

        h6 {
            text-transform: uppercase;
            margin-top: 8px;
        }

        .item .item-title h6 {
            margin-top: 10px;
            font-size: 16px;
            min-height: 50px;
        }

        .item-title h5 {
            font-size: 18px;
        }

        .item .item-title h6:hover, h5:hover {
            color: #FF6633;
        }
    </style>
</head>

<body>
<header>
    <jsp:include page="/WEB-INF/views/front-end/common/header.jsp"></jsp:include>
    <div class="bg-black-menu">
        <div class="container">
            <div class="menu">
					<span id="icon-close" class="d-md-none"><i
                            class="fas fa-times"></i></span>
                <ul>
                    <li>
                        <div class="search d-flex d-md-none d-block">
                            <div>
                                <input type="text" placeholder="Tìm kiếm...">
                            </div>
                            <div>
                                <button>
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </li>
                    <li class="active"><a
                            href="${pageContext.request.contextPath}/">Trang Chủ</a></li>
                    <li><a href="${pageContext.request.contextPath}/gioithieu">Giới
                        Thiệu</a></li>
                    <li><a href="${pageContext.request.contextPath}/quanAo">Sản
                        phẩm</a></li>
                    <li><a href="${pageContext.request.contextPath}/blog">Blogs</a></li>
                    <li><a href="${pageContext.request.contextPath}/contact">Liên
                        Hệ</a></li>
                </ul>
            </div>
        </div>
    </div>
    </div>
    <div id="carouselExampleIndicators" class="carousel slide banner"
         data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0"
                class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active ">
                <img class="d-block w-100 banner-image" 48
                     src="../image/banner.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="../image/banner2.jpg"
                     alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="../image/clothes-famous-black-banner.jpg"
                     alt="Second slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators"
           role="button" data-slide="prev"> <span
                class="carousel-control-prev-icon" aria-hidden="true"></span> <span
                class="sr-only">Previous</span>
        </a> <a class="carousel-control-next" href="#carouselExampleIndicators"
                role="button" data-slide="next"> <span
            class="carousel-control-next-icon" aria-hidden="true"></span> <span
            class="sr-only">Next</span>
    </a>
    </div>
</header>
<br>
<div class="container desc">
    <h4>Ưu đãi & Khuyến mãi </h4>
    <div class="row mt-5 d-flex justify-content-between">
        <div class="filler col-md-4 col-12" data-aos="zoom-in-up">
            <div class="con">
                <div class="img-content mr-3">
                    <img src="../image/uudai1.jpg" height="250px" width="100%">
                </div>
                <div class="text-inner">
                    <hr/>
                </div>
            </div>
        </div>
        <div class="filler col-md-4 col-12 pt-3 pt-md-0"
             data-aos="zoom-in-up">
            <div class="con">
                <div class="img-content">
                    <img src="../image/uudai2.jpg" height="250px" width="100%">
                </div>
                <div class="text-inner">
                    <hr/>
                </div>
            </div>
        </div>
        <div class="filler col-md-4 col-12 pt-3 pt-md-0"
             data-aos="zoom-in-up">
            <div class="con">
                <div class="img-content">
                    <img src="../image/uudai3.png" height="250px" width="100%">
                </div>
                <div class="text-inner">
                    <hr/>
                </div>
                5
            </div>
        </div>
    </div>
</div>

<nav class="container menu-tab">
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-home-tab"
           data-toggle="tab" href="#nav-home" role="tab"
           aria-controls="nav-home" aria-selected="true">Mẫu xe mới nhất</a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab"
           href="#nav-profile" role="tab" aria-controls="nav-profile"
           aria-selected="false">Mẫu xe giảm giá </a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active container" id="nav-home"
         role="tabpanel" aria-labelledby="nav-home-tab">

        <div class="row container-items">
            <c:forEach var="product" items="${products}" begin="0" end="5">
                <div class="item col-12 col-md-6 col-xl-3 p-2 m-0">

                    <div class="item-image">
                        <c:forEach var="productImages" end="0"
                                   items="${product.productImages}">
                            <img src="../file/upload/${productImages.title}" width="100%"
                                 height="300px"/>
                        </c:forEach>
                        <div class="bttn">
                            <button class="btn bttn-l" data-toggle="modal"
                                    data-target="#exampleModal"
                                    onclick="Cart.gioHang(${product.id},1);">Thêm vào giỏ
                            </button>
                            <a class="btn bttn-l"
                               href="${pageContext.request.contextPath}/product-detail/${product.seo}">Xem
                                chi tiết</a>
                        </div>
                    </div>
                    <div class="item-title">
                        <a
                                href="${pageContext.request.contextPath}/product-detail/${product.seo}">
                            <h5 class="text-decoration-none">${product.title}</h5> <c:if
                                test="${product.priceSale != null}">
                            <div class="cost d-flex justify-content-center">
                                <div class="pr-2 cost1">
                                    <fmt:formatNumber type="number" maxIntegerDigits="13"
                                                      value="${product.priceSale}"/>
                                    đ
                                </div>
                                <div class="text-muted ">
                                    <s><fmt:formatNumber type="number" maxIntegerDigits="13"
                                                         value="${product.price}"/> đ</s>
                                </div>
                                <div class="desc-sale">
                                    <span> -${product.percentSale}%</span>
                                </div>
                            </div>
                        </c:if> <c:if test="${product.priceSale == null}">
                            <div class="cost cost1">
                                <fmt:formatNumber type="number" maxIntegerDigits="13"
                                                  value="${product.price}"/>
                                đ
                            </div>
                        </c:if>
                        </a>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
    <div class="tab-pane fade container" id="nav-profile" role="tabpanel"
         aria-labelledby="nav-profile-tab">

        <div class="row container-items">
            <c:forEach var="product" items="${productsSale}" begin="0" end="5">
                <div class="item col-3 p-2 m-0">
                    <div class="item-image">
                        <c:forEach var="productImages" end="0"
                                   items="${product.productImages}">
                            <img src="../file/upload/${productImages.title}" width="100%"
                                 height="300px"/>
                        </c:forEach>
                        <div class="bttn">
                            <button class="btn bttn-l" data-toggle="modal"
                                    data-target="#exampleModal"
                                    onclick="Cart.gioHang(${product.id},1);">Thêm vào giỏ
                            </button>
                            <a class="btn bttn-l"
                               href="${pageContext.request.contextPath}/product-detail/${product.seo}">Xem
                                chi tiết</a>
                        </div>
                    </div>
                    <div class="item-title">
                        <a
                                href="${pageContext.request.contextPath}/product-detail/${product.seo}">
                            <h5 class="text-decoration-none">${product.title}</h5> <c:if
                                test="${product.priceSale != null}">
                            <div class="cost d-flex justify-content-center">
                                <div class="pr-2 cost1">
                                    <fmt:formatNumber type="number" maxIntegerDigits="13"
                                                      value="${product.priceSale}"/>
                                    đ
                                </div>
                                <div class="text-muted ">
                                    <s><fmt:formatNumber type="number" maxIntegerDigits="13"
                                                         value="${product.price}"/> đ</s>
                                </div>
                                <div class="desc-sale">
                                    <span> -${product.percentSale}%</span>
                                </div>
                            </div>
                        </c:if> <c:if test="${product.priceSale == null}">
                            <div class="cost cost1">
                                <fmt:formatNumber type="number" maxIntegerDigits="13"
                                                  value="${product.price}"/>
                                đ
                            </div>
                        </c:if>
                        </a>
                    </div>
                </div>

            </c:forEach>
        </div>
    </div>
</div>
<div class="container infor">
    <div class="row mt-5 d-flex justify-content-between">
        <div class="filler col-md-4 col-12" data-aos="fade-right">
            <div class="con">
                <div class="img-content mr-3">
                    <img src="../image/quan_shorts_new.jpg" height="250px" width="100%">
                </div>
            </div>
            <div class="text-outer">
                <h5>Quần shorts nam Classic</h5>
                <p>Nếu bạn đang tìm cho mình một chiếc quần để mặc đi biển, hay để tham gia các hoạt động ngoài trời thì
                    bạn không thể bỏ lỡ chiếc <strong>Quần Shorts nam Classic Beach&nbsp;</strong></p>
            </div>
        </div>
        <div class="filler col-md-4 col-12 pt-3 pt-md-0" data-aos="fade-up">
            <div class="con">
                <div class="img-content">
                    <img src="../image/tintuc2.jpg " height="250px" width="100%">
                </div>

            </div>
            <div class="text-outer">
                <h5>Quần Jeans công nghệ mới.</h5>
                <p><strong>Dòng Sản Phẩm
                        Jeans Clean Denim - với nguyên liệu và quy cách nhuộm mới đảm bảo sẽ là chiếc Jeans bền màu và
                        thân thiện với môi trường.</strong></p>
            </div>
        </div>
        <div class="filler col-md-4 col-12 pt-3 pt-md-0" data-aos="fade-left">
            <div class="con">
                <div class="img-content">
                    <img src="../image/tintuc3.png" height="250px" width="100%">
                </div>
            </div>
            <div class="text-outer">
                <h5>Công Nghệ Excool Là gì?</h5>
                <p><b style="color: rgb(2, 173, 239);">Excool</b> là kết quả của gần 2 năm nghiên cứu và phát
                    triển, liên tục thử nghiệm chất liệu mới của Coolmate và đối tác phát triển vải và
                    Dupont (tập đoàn hóa chất may mặc số #1 thế giới). <a
                            href="https://www.coolmate.me/post/soi-sorona-la-gi" style="text-decoration: underline;">.
                </p>
            </div>
        </div>
    </div>
</div>

<jsp:include page="/WEB-INF/views/front-end/common/footer.jsp"></jsp:include>
</body>
<jsp:include page="/WEB-INF/views/front-end/common/js.jsp"></jsp:include>
</html>