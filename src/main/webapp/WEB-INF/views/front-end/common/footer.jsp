<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<div class="container">
		<div class="content-footer">
			<div class="left float-left">
				<h3>Đăng kí nhận thông tin</h3>
			</div>
			<div class="right float-right">
				<form class="form-inline"
					action="${pageContext.request.contextPath}/" method="post">

					<div class="input-group mb-2 mr-sm-2">
						<input type="text" class="form-control" id="emailfield" name="emailfield"
							placeholder="Email" />
					</div>
					<button type="submit" class="btn btn-info mb-2">Subcribe</button>
				</form>
			</div>
		</div>
	</div>
<footer>
		<div class="container-fluid bg-black">
		<div class="container clearfix footer2">
			<div class="infor">
				<h3 class="text-light">Thông tin liên hệ</h3>
				<ul>
					<li><a href="#">
						<i class="fas fa-map-marker-alt"></i>
						Triều Khúc, Hà Nội</a>
					</li>
					<li><a href="#">
						<i class="fas fa-phone"></i>
						0987654321</a>
					</li>
					<li><a href="#">
						<i class="fas fa-envelope"></i>
						email@gmail.com</a>
					</li>
					<li><a href="#">
						<i class="fab fa-facebook-square"></i>
						Clothes</a>
					</li>
				</ul>
			</div>
			<div class="infor">
				<h3 class="text-light">Liên kết</h3>
				<ul>
					<li class="active"><a href="${pageContext.request.contextPath}/">Trang Chủ</a></li>
					<li><a href="${pageContext.request.contextPath}/gioithieu">Giới Thiệu</a></li>
					<li><a href="${pageContext.request.contextPath}/quanAo">Shop quần áo</a></li>
					<li><a href="${pageContext.request.contextPath}/blog">Blogs</a></li>
					<li><a href="${pageContext.request.contextPath}/contact">Liên Hệ</a></li>		
				</ul>
			</div>
			<div class="infor">
				<h3 class="text-light">Hỗ trợ</h3>
				<ul>
					<li><a href="#">Hướng dẫn mua hàng</a></li>
					<li><a href="#">Hướng dẫn thanh toán</a></li>
					<li><a href="#">Chính sách bảo hành</a></li>
					<li><a href="#">Chính sách đổi trả</a></li>
					<li><a href="#">Tư vấn khách hàng</a></li>
				</ul>
			</div>
			<div class="infor">
				<h3 class="text-light">Tải ứng dụng trên</h3>
				<p>Ứng dụng Mona Watch có sẵn trên Google Play & App Store. Tải nó ngay.</p>
				<ul>
					<li><a href="#">
						<img src="../image/img-googleplay.jpg"></a>
					</li>
					<li><a href="#">
						<img src="../image/img-appstore.jpg"></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="container">
			<div class="coppyright">
				<i class="far fa-copyright"></i>
				Không có bản quyền
			</div>
		</div>
		</div>
	</footer>
	<div class="social d-none d-lg-block">
		<a href="https://www.facebook.com"><i
			class="fab fa-facebook-f"></i></a> <a
			href="https://www.facebook.com"><i
			class="fab fa-twitter"></i></a> <a
			href="https://www.instagram.com"><i
			class="fab fa-instagram"></i></a> <a
			href="https://www.youtube.com"><i
			class="fab fa-youtube"></i></a>
	</div>
	<a class="on_top" href="#top" style="display: block;"><i class="fa-arrow-circle-up fa"></i></a>
	
	<div class="hotline-phone-ring-wrap">
    <div class="hotline-phone-ring">
        <div class="hotline-phone-ring-circle"></div>
        <div class="hotline-phone-ring-circle-fill"></div>
        <div class="hotline-phone-ring-img-circle">
        <a href="tel:0386950395" class="pps-btn-img">
            <img src="https://nocodebuilding.com/wp-content/uploads/2020/07/icon-call-nh.png" alt="Gọi điện thoại" width="50">
        </a>
        </div>
    </div>
    <div class="hotline-bar">
        <a href="tel:0386950395">
            <span class="text-hotline">0987654321</span>
        </a>
    </div>
</div>